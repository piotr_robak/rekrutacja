<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rekrutacja</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="https://use.fontawesome.com/7727a684a1.js"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="{{ asset('js/app.js') }}"></script>
        <style>
            .vcenter{
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .product-heading{
                font-size: 25px;
                font-weight:bold ;
                margin-bottom: 10px;
            }
            .product{
                cursor: pointer;
            }
            .inline-block{
                display: inline-block;
            }
            .product-description{
                margin-top: 15px;
                margin-bottom: 15px;
            }
        </style>
        @yield('script')
    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}">Rekrutacja</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ route('home') }}">Produkty</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
       @yield('content')
    </div>
    </body>
</html>
