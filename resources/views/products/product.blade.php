<div class="list-group product">
    <div class="list-group-item">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="list-group-item-heading product-heading inline-block">{{ $name }}</h4>
                <div class="dropdown inline-block pull-right">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{ $prices->first()->price }} @if($prices->count() <= 1) </button> @endif
                    @if($prices->count() > 1)
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            @foreach($prices as $key => $price)
                                @if($key != 0)
                                    <li><a href="#">{{ $price->price }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </div>
                <p class="list-group-item-text text-justify product-description">{{ $description }}</p>
                <div class="btn-group btn-group-sm pull-right">
                    <a class="btn btn-warning edit-product btn-sm" href="{{ route('edit-dialog', $id) }}">Edytuj <i class="fa fa-edit"></i></a>
                    <button class="btn btn-danger remove-product btn-sm" data-product="{{ $id }}">Usuń <i class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>