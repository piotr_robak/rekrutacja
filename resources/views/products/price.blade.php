<div class="form-group">
    <label>Cena</label>
    <div class="input-group">
        <input class="form-control" name="old_prices[{{ $id }}]" placeholder="Podaj cenę produktu" value="{{ $price }}" pattern="^\d*(\.)?(\d*)?$" required>
        <div class="input-group-btn">
            <button class="btn btn-danger remove-price" type="button">Usuń cenę <i class="fa fa-remove"></i></button>
        </div>
    </div>
</div>