<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Usuń produkt</h4>
    </div>
    <div class="modal-body">
        <p>Czy na pewno chcesz usunąć produkt "{{ $product->name }}"?</p>
    </div>
    <div class="modal-footer">
        <form action="{{ route('remove', $product->id) }}" method="post">
            {{ csrf_field() }}
            <button type="button" class="btn btn-primary" data-dismiss="modal">Anuluj <i class="fa fa-remove"></i></button>
            <button class="btn btn-danger pull-right">Usuń <i class="fa fa-trash"></i></button>
        </form>
    </div>
</div>