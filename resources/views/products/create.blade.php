@extends('home')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Dodaj produkt<a class="btn btn-default pull-right" href="{{ route('home') }}">Cofnij <i class="fa fa-undo"></i></a></h2>
                </div>
                <form action="{{ route('store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nazwa</label>
                                <input class="form-control" name="name" placeholder="Podaj nazwę produktu" required>
                            </div>
                            <div class="form-group">
                                <label>Opis</label>
                                <textarea class="form-control" rows="4" placeholder="Podaj opis produktu" name="description" required></textarea>
                            </div>
                            <div id="price-template">
                                <div class="form-group">
                                    <label>Cena</label>
                                    <input class="form-control" name="prices[]" placeholder="Podaj cenę produktu" pattern="^\d*(\.)?(\d*)?$" required>
                                </div>
                            </div>
                            <div id="prices"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-default pull-right" id="add-price">Dodaj cenę <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pull-right btn-lg btn-block">Dodaj <i class="fa fa-send"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script>
        $(document).ready(function(){
            $('.panel-body').on('click','.remove-price',function(){
               $(this).parents('.form-group').remove();
            });
            $('#add-price').click(function () {
                $('#prices').append('<div class="form-group"><label>Cena</label><div class="input-group"><input class="form-control" name="prices[]"  name="prices[]" placeholder="Podaj cenę produktu" pattern="^\\d*(\\.)?(\\d*)?$" required><div class="input-group-btn"><button type="button" class="btn btn-danger remove-price">Usuń cenę <i class="fa fa-remove"></i></button></div></div>');
            });
        });
    </script>
@endsection