@extends('home')
@section('content')
    <div id="product-dialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
        </div>
    </div>
    <div class="row">
        @if(session('status'))
            <div class="col-sm-12 message">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="col-sm-12">
            <div class="page-header">
                <h1>
                    Lista produktów
                    <small class="pull-right">
                        <a class="btn btn-primary pull-right" href="{{ route('create') }}">Dodaj Produkt <i class="fa fa-plus"></i></a>
                    </small>
                </h1>
            </div>
        </div>
        <div class="col-sm-12">
            @if($products->count() > 0)
                @foreach($products as $product)
                    @component('products.product', ['name' => $product->name, 'prices' => $product->product_prices, 'description' => $product->description, 'id' => $product->id])
                    @endcomponent
                @endforeach
            @else
                <div class="text-center vcenter">
                    <h2>Brak produktów</h2>
                </div>
            @endif
        </div>
        <div class="col-sm-12 text-right">
            {{ $products->links() }}
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script>
        $(document).ready(function(){
            setTimeout(function() {
                $('.message').fadeOut('slow');
            }, 3500);
            $('.remove-product').click(function () {
               $.get('remove-dialog/'+ $(this).data('product'), function(data){
                    $('.modal-dialog').html(data);
                    $('#product-dialog').modal();
               });
            });

        })
    </script>
@endsection
