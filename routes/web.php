<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                     'ProductController@index')->name('home');
Route::get('/create',               'ProductController@create')->name('create');
Route::post('/store',               'ProductController@store')->name('store');
Route::get('/remove-dialog/{id}',   'ProductController@removeDialog');
Route::post('/remove/{id}',         'ProductController@remove')->name('remove');
Route::get('/edit-dialog/{id}',     'ProductController@editDialog')->name('edit-dialog');
Route::post('/edit/{id}',           'ProductController@edit')->name('edit');
