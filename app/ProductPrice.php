<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{
    protected $fillable = [
        'product_id',
        'price'
    ];

    protected $dates = ['deleted_at'];

    use SoftDeletes;
}
