<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    protected $dates = ['deleted_at'];

    use SoftDeletes;

    public function prices(){
        return $this->hasMany(ProductPrice::class);
    }

    public function getProductPricesAttribute(){
        return $this->prices()->orderBy('price', 'asc')->get();
    }
}
