<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductPrice;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $products = Product::with('prices')->simplePaginate(5);
        return view('products.index', compact('products'));
    }

    public function create(){
        return view('products.create');
    }

    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'description' => 'required',
            'prices.*' => 'required|numeric'
        ]);
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        foreach($request->prices as $price){
            ProductPrice::create([
                'product_id' => $product->id,
                'price' => str_replace(',', '.',$price)
            ]);
        }
        return redirect()->route('home')->with('status', 'Produkt został dodany pomyślnie');
    }

    public function removeDialog($id){
        $product = Product::find($id);
        return view('products.remove', compact('product'));
    }

    public function remove($id){
        Product::destroy($id);
        return redirect()->back()->with('status', 'Produkt został usunięty pomyślnie');
    }

    public function editDialog($id){
        $product = Product::with('prices')->find($id);
        return view('products.edit', compact('product'));
    }

    public function edit($id, Request $request){

        //  SPECJALNIE ZROBIONE W TEN SPOSOB, GDYBY DO CENY DOLACZONA BYLA JESZCZE JAKAS KOLUMNA! PONIEWAZ MOZNABY USUNAC WSZYSTKIE CENY I WKLEIC TE KTORE SA W REQUESCIE

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'description' => 'required',
            'prices.*' => 'required|numeric',
            'old_prices.*' => 'required|numeric'
        ]);
        $product = Product::with('prices')->find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->save();

        if($request->has('old_prices')){
            $request_prices = [];
            foreach($request->old_prices as $key => $request_price){
                $request_prices[] = $key;
                $price = ProductPrice::find($key);
                if($price->price != $request_price) {
                    $price->price = $request_price;
                    $price->save();
                }
            }
            $to_delete = array_diff($product->prices()->pluck('id')->toArray(), $request_prices);
            ProductPrice::destroy($to_delete);
        }else{
            foreach($product->prices as $price){
                ProductPrice::destroy($price->id);
            }
        }

        if($request->has('prices')){
            foreach($request->prices as $price){
                ProductPrice::create([
                    'product_id' => $product->id,
                    'price' => str_replace(',', '.',$price)
                ]);
            }
        }

        return redirect()->route('home')->with('status', 'Produkt został dodany pomyślnie');
    }
}
